/*jslint evil: true */
/**
    WYMeditor.alignment
    ====================

    A plugin to add a class to a container which can be used to set the alignment
	for it.
	
	by Patabugen ( patabugen.co.uk )
*/

WYMeditor.editor.prototype.alignment = function () {
    var wym = this,
		$box = jQuery(this._box);
		
	options = {
		
	}

	//construct the buttons' html
    var button_left = String() +
        "<li class='wym_tools_alignment_left'>" +
            "<a name='AlignLeft' href='#' " +
                "style='background-image: url(" +
                    wym._options.basePath +
                    "plugins/alignment/icons.png)'>" +
                "{left}" +
            "</a>" +
        "</li>";
    var button_center = String() +
        "<li class='wym_tools_alignment_center'>" +
            "<a name='AlignCenter' href='#' " +
                "style='background-image: url(" +
                    wym._options.basePath +
                    "plugins/alignment/icons.png); background-position: 0px -24px'>" +
                "{Center}" +
            "</a>" +
        "</li>";
    var button_right = String() +
        "<li class='wym_tools_alignment_right'>" +
            "<a name='AlignRight' href='#' " +
                "style='background-image: url(" +
                    wym._options.basePath +
                    "plugins/alignment/icons.png); background-position: 0px -48px'>" +
                "{right}" +
            "</a>" +
        "</li>";
    var button_justify = String() +
        "<li class='wym_tools_alignment_justify'>" +
            "<a name='AlignJustify' href='#' " +
                "style='background-image: url(" +
                    wym._options.basePath +
                    "plugins/alignment/icons.png); background-position: 0px -72px'>" +
                "{justify}" +
            "</a>" +
        "</li>";
		
	var html = button_left + button_center + button_right + button_justify;
    //add the button to the tools box
    $box.find(wym._options.toolsSelector + wym._options.toolsListSelector)
        .append(html);
		

  function get_nearest_paragraph(current_container)
  {
    current_container = typeof current_container !== 'undefined' ? current_container : $(wym.container());
    if (current_container.hasClass('wym_iframe_body'))
      throw "alignment plugin has reached the top of wym_iframe_body without finding a paragraph";
    if (!(current_container.is('p')))
      return (get_nearest_paragraph(current_container.parent()));
    return (current_container);
  }

    $box.find('li.wym_tools_alignment_left a').click(function() {
		var container = get_nearest_paragraph();
		$(container).removeClass('align_left align_right align_justify align_center');
		$(container).addClass('align_left');
		return false;
	});
    $box.find('li.wym_tools_alignment_center a').click(function() {
		var container = get_nearest_paragraph();
		$(container).removeClass('align_left align_right align_justify align_center');
		$(container).addClass('align_center');
		return false;
	});
    $box.find('li.wym_tools_alignment_right a').click(function() {
		var container = get_nearest_paragraph();
		$(container).removeClass('align_left align_right align_justify align_center');
		$(container).addClass('align_right');
		return false;
	});
    $box.find('li.wym_tools_alignment_justify a').click(function() {
		var container = get_nearest_paragraph();
		$(container).removeClass('align_left align_right align_justify align_center');
		$(container).addClass('align_justify');
		return false;
	});
};
